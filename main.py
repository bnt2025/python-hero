#!/usr/bin/python3

import cv2
from datetime import datetime
from time import time, sleep
import numpy as np


GREEN = (0, 255, 0)
RED = (0, 0, 255)
YELLOW = (0, 255, 255)
BLUE = (255, 0, 0)


class PythonHero():
    masks = {}
    video_dimen = None
    threshold = 0.8
    detected_count = 0

    def __init__(self):
        self.template_green = cv2.imread('gems/green_1.png', 0)
        self.template_width, self.template_height = self.template_green.shape[:2]

    def load_masks(self):
        self.load_mask("GREEN", 'masks/lane_green.png',
                       GREEN, 'gems/green_1.png')
        self.load_mask("RED", 'masks/lane_red.png', RED, 'gems/red_0.png')
        self.load_mask("YELLOW", 'masks/lane_yellow.png',
                       YELLOW, 'gems/green_1.png')

    def load_mask(self, key, path, color, template):
        mask = cv2.imread(path)
        height, width = mask.shape[:2]
        if self.video_dimen[1] != height or self.video_dimen[0] != width:
            print("[-] Video and mask are different sizes, resizing mask.")
            mask = cv2.resize(mask,
                              (self.video_dimen[1], self.video_dimen[0]),
                              interpolation=cv2.INTER_AREA)
            height, width = mask.shape[:2]
        imgray = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(imgray, 127, 255, 0)[1]
        contours, hierarchy = cv2.findContours(thresh,
                                               cv2.RETR_TREE,
                                               cv2.CHAIN_APPROX_SIMPLE)
        for contour, hier in zip(contours, hierarchy):
            (x, y, w, h) = cv2.boundingRect(contour)
            self.masks[key] = {
                "TL": (x, y),
                "BR": (x+w, y+h),
                "ROI_X": [x, x+w],
                "ROI_Y": [y, y+h],
                "Color": color
            }
        self.masks[key]["template"] = cv2.imread(template, 0)

    def process_video(self):
        cap = cv2.VideoCapture('test_videos/lou.mp4')
        # Check if camera opened successfully
        if (cap.isOpened() == False):
            print("[-] Error opening video stream or file")
        frame_count = 1
        # Read until video is completed
        while(cap.isOpened()):
            # Capture frame-by-frame
            ret, frame = cap.read()
            if ret == True:

                if self.video_dimen is None:
                    self.video_dimen = frame.shape[:2]
                    print(
                        "[+] Video dimensions {} x {}".format(self.video_dimen[0], self.video_dimen[1]))
                    self.load_masks()

                frame_count += 1
                cv2.imshow('Frame', self.process_frame(frame))

                # Press Q on keyboard to exit
                # Speed through the first section of the video for test
                if cv2.waitKey(100 if frame_count > 650 else 1) & 0xFF == ord('q'):
                    break
            else:
                break

        # When everything done, release the video capture object
        cap.release()
        # Closes all the frames
        cv2.destroyAllWindows()

    def process_frame(self, image):

        img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        for key in self.masks:

            thickness = 2
            # We only care about a small area so we set the Region Of Intrest here
            roi = img_gray[self.masks[key]["ROI_Y"][0]:self.masks[key]["ROI_Y"][1],
                           self.masks[key]["ROI_X"][0]:self.masks[key]["ROI_X"][1]]

            res = cv2.matchTemplate(roi,
                                    self.masks[key]["template"],
                                    cv2.TM_CCOEFF_NORMED)

            for pt in zip(*np.where(res >= self.threshold)[::-1]):
                thickness = -1
                # Convert the point coords backinto the raw image, instead of coords from the ROI
                # pt = (pt[0]+self.masks[key]["ROI_X"][0],
                #       pt[1]+self.masks[key]["ROI_Y"][0])
                # cv2.rectangle(image,
                #               pt,
                #               (pt[0] + self.template_width,
                #                pt[1] + self.template_height),
                #               self.masks[key]["Color"],
                #               2)
                self.detected_count += 1

            cv2.rectangle(image,
                          (self.masks[key]["TL"]),
                          (self.masks[key]["BR"]),
                          self.masks[key]["Color"],
                          thickness)

        return image


if __name__ == "__main__":
    p = PythonHero()
    p.process_video()
