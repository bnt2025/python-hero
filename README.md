# python-hero

An attempt at using Python and OpenCV to play Guitar Hero for me.

I downloaded some videos from Youtube of Guitar Hero, as example captures.

## Installations

### Windows

- Download and install Python 3 from [https://www.python.org/downloads/](homepage)
- Download OpenCV precompiled wheel for Windows from [https://www.lfd.uci.edu/~gohlke/pythonlibs/#opencv]()
- Open your Downloads folder in Windows Explorer.
- Make sure no files or folders are selected and hold Shift and right click in a blank space in the folder.
- In the Right Click menu select `Open Powershell windows here`.

```ps
py -m pip install --upgrade pip
py -m pip install .\opencv_python-4.2.0-cp38-cp38-win_amd64.whl
py -m pip install opencv-contrib-python
py -m pip install numpy
py -m pip install imutils
py -m pip install scikit-learn
```

change the opencv filename as required|

### Ubuntu

Easy.

```bash
sudo apt install -y python3 python3-opencv python3-pip
sudo -H pip3 install numpy
```

## Useful links

- [https://www.pyimagesearch.com/2020/01/20/intro-to-anomaly-detection-with-opencv-computer-vision-and-scikit-learn/]()

## TODO

- Get it actually working
- Do detection from a webcam.
- Make an Arduino guitar interface.
- Send command to Arudino when gem has been detected.
- Use OpenCV with CUDA.
- Do it for Rock Band as well.
